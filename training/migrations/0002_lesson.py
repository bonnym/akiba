# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-21 10:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('training', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('cover', models.FileField(upload_to='uploads/lessons/')),
                ('video', models.URLField()),
                ('description', models.TextField()),
            ],
        ),
    ]
