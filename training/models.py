from django.db import models


# Questionare for marketing

class Product(models.Model):
    """
    Description: Model Description
    """
    quiz = models.CharField(max_length=300)
    cover = models.FileField()
    image = models.FileField()
    video = models.URLField(max_length=200)
    description = models.TextField()

    class Meta:
        pass


class Price(models.Model):
    """
    Description: Model Description
    """
    quiz = models.CharField(max_length=300)
    cover = models.FileField()
    image = models.FileField()
    video = models.URLField(max_length=200)
    description = models.TextField()

    class Meta:
        pass


class Place(models.Model):
    """
    Description: Model Description
    """
    quiz = models.CharField(max_length=300)
    cover = models.FileField()
    image = models.FileField()
    video = models.URLField(max_length=200)
    description = models.TextField()

    class Meta:
        pass


class Promotion(models.Model):
    """
    Description: Model Description
    """
    quiz = models.CharField(max_length=300)
    cover = models.FileField()
    image = models.FileField()
    video = models.URLField(max_length=200)
    description = models.TextField()

    class Meta:
        pass


# Lesson
class Lesson(models.Model):
    """
    Description: Model Description
    """
    title = models.CharField(max_length=200)
    cover = models.FileField(upload_to='uploads/lessons/', max_length=100, null=True)
    video = models.URLField(max_length=200, null=True)
    description = models.TextField()
    image = models.FileField(upload_to='uploads/lessons/', max_length=100, null=True)

    class Meta:
        pass
