from django.contrib import admin
from .models import *


# class Admin(admin.ModelAdmin):
#     '''
#         Admin View for 
#     '''
#     list_display = ('',)
#     list_filter = ('',)
#     inlines = [
#         Inline,
#     ]
#     raw_id_fields = ('',)
#     readonly_fields = ('',)
#     search_fields = ('',)

admin.site.register(Lesson)
admin.site.register(Product)
admin.site.register(Price)
admin.site.register(Place)
admin.site.register(Promotion)
